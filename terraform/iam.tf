# resource aws_iam_role iam_for_s3_to_sqs_lambda
resource "aws_iam_role" "iam_for_s3_to_sqs_lambda" {
  name = var.s3_to_sqs_lambda_role_name

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}


# resource aws_iam_role_policy lambda_logging_for_s3_to_sqs
resource "aws_iam_role_policy" "lambda_logging_for_s3_to_sqs" {
  name   = "lambda_logging_for_s3_to_sqs"
  role   = aws_iam_role.iam_for_s3_to_sqs_lambda.id
  policy = file("files/log_policy.json")
}

# resource aws_iam_role_policy iam_policy_for_sqs_sender
resource "aws_iam_role_policy" "iam_policy_for_sqs_sender" {
  name = "iam_policy_for_sqs_sender"
  role = aws_iam_role.iam_for_s3_to_sqs_lambda.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : "sqs:SendMessage",
          "Resource" : "${aws_sqs_queue.job_offers_queue.arn}"
        }
      ]
    }
  )
}
# resource aws_iam_role_policy iam_policy_for_s3
resource "aws_iam_role_policy" "iam_policy_for_s3" {
  name = "iam_policy_for_s3"
  role = aws_iam_role.iam_for_s3_to_sqs_lambda.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : "s3:*",
          "Resource" : "${aws_s3_bucket.S3Bucket.arn}/*"
        }
      ]
  })

}

# resource aws_iam_role iam_for_sqs_to_dynamo_lambda
resource "aws_iam_role" "iam_for_sqs_to_dynamo_lambda" {
  name = var.sqs_to_dynamo_lambda_role_name

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

# resource aws_iam_role_policy lambda_logging_for_sqs_to_dynamo
resource "aws_iam_role_policy" "lambda_logging_for_sqs_to_dynamo" {
  name   = "lambda_logging_for_sqs_to_dynamo"
  role   = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id
  policy = file("files/log_policy.json")
}

# resource aws_iam_role_policy iam_policy_for_dynamodb
resource "aws_iam_role_policy" "iam_policy_for_dynamodb" {
  name = "iam_policy_for_dynamodb"
  role = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : "sqs:ReceiveMessage",
          "Action" : "sqs:DeleteMessage",
          "Action" : "sqs:GetQueueAttributes"
          "Resource" : "${aws_lambda_function.sqs_to_dynamo_lambda.arn}"
      }]
  })
}

# resource aws_iam_role_policy iam_policy_for_sqs_receiver
resource "aws_iam_role_policy" "iam_policy_for_sqs_receiver" {
  name = "iam_policy_for_sqs_receiver"
  role = aws_iam_role.iam_for_sqs_to_dynamo_lambda.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : "dynamodb:PutItem",
          "Resource" : "${aws_dynamodb_table.job-table.arn}"
      }]
  })
}

# resource aws_iam_role_policy_attachment sqs_exec_policy_attach
resource "aws_iam_role_policy_attachment" "sqs_exec_policy_attach" {
  role       = aws_iam_role.iam_for_sqs_to_dynamo_lambda.name
  policy_arn = var.sqs_exec_policy_arn
}

# resource aws_iam_role iam_for_job_api_lambda
resource "aws_iam_role" "iam_for_job_api_lambda" {
  name = var.job_api_lambda_role_name

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

# resource aws_iam_role_policy iam_policy_for_dynamodb
resource "aws_iam_role_policy" "iam_policy_for_job_api_dynamodb" {
  name = "iam_policy_for_dynamodb"
  role = aws_iam_role.iam_for_job_api_lambda.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : "dynamodb:*",
          "Resource" : "${aws_apigatewayv2_api.job_api_gw.execution_arn}"
        }
      ]
    }
  )
}

# resource aws_iam_role_policy lambda_logging_for_job_api
resource "aws_iam_role_policy" "lambda_logging_for_job_api" {
  name   = "lambda_logging_for_job_api"
  role   = aws_iam_role.iam_for_job_api_lambda.id
  policy = file("files/log_policy.json")
}

# resource aws_iam_role_policy_attachment lambda_exec_policy_attach
resource "aws_iam_role_policy_attachment" "lambda_exec_policy_attach" {
  role       = aws_iam_role.iam_for_job_api_lambda.name
  policy_arn = var.lambda_exec_policy_arn
}

resource "aws_iam_role_policy" "job_api_lambda_db_scan" {
  name = "job_api_lambda_db_scan"
  role = aws_iam_role.iam_for_job_api_lambda.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : "dynamodb:Scan",
          "Resource" : "${aws_dynamodb_table.job-table.arn}"
      }]
  })
}