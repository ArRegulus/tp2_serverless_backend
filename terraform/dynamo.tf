resource "aws_dynamodb_table" "job-table" {
  name           = var.dynamo_job_table_name
  billing_mode   = "PROVISIONED"
  hash_key       = "id"
  range_key      = "city"
  read_capacity  = 5
  write_capacity = 5

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "city"
    type = "S"
  }
}
