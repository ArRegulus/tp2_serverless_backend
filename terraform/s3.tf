# resource aws_s3_bucket s3_job_offer_bucket
resource "aws_s3_bucket" "S3Bucket" {
  bucket        = var.s3_user_bucket_name
  force_destroy = true
}

# resource aws_s3_object job_offers
resource "aws_s3_object" "folder" {
  bucket        = aws_s3_bucket.S3Bucket.id
  key           = "job_offers/"
  force_destroy = true
}

# resource aws_s3_bucket_notification bucket_notification
resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.S3Bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = aws_s3_object.folder.key
    filter_suffix       = ".csv"
  }
}

